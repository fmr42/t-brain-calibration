#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
import numpy as np
import sys
import pyrealsense2 as rs


width=1920 # image width of the realsense camera. Nominal: 1920
height=1080 # image height of the realsense camera. Nominal: 1080
rsfps=30 # fps target for realsense camera. Nominal: 30

print("Getting pipeline...")
pipeline = rs.pipeline()
print("Getting config...")
config = rs.config()
print("Enabling stream...")
config.enable_stream(rs.stream.color, width, height, rs.format.bgr8, rsfps) # Nominal: 1920x18080 bgr8 30fps
print("Resolving pipeline...")
profile = config.resolve(pipeline)
print("Starting pipeline...")
pipeline.start(config)
num_frames=0

while True :
    frames = pipeline.wait_for_frames()
    color_frame = frames.get_color_frame()
    if not color_frame:
        num_frames=0
        continue
    image_np = np.asanyarray(color_frame.get_data())
    image_np = cv2.flip(image_np, -1)
    cv2.imshow('CamSpace', cv2.cvtColor(image_np, cv2.COLOR_RGB2BGR))
    if (cv2.waitKey(500)>0):
	    break
    
pipeline.stop()



sys.exit(0)

