#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
import numpy as np
import sys
import pyrealsense2 as rs


if (len(sys.argv) != 9):
    print("Usage:")
    print(sys.argv[0] , " <top right x> <top right y> <top left x> <top left y> <bottom left x> <bottom left y> <bottom right x> <bottom right y>")
    sys.exit(1)


pt0WorkSpace = [float(sys.argv[1]) , float(sys.argv[2]) ]
pt1WorkSpace = [float(sys.argv[3]) , float(sys.argv[4]) ]
pt2WorkSpace = [float(sys.argv[5]) , float(sys.argv[6]) ]
pt3WorkSpace = [float(sys.argv[7]) , float(sys.argv[8]) ]
ptsWorkSpace = [pt0WorkSpace,pt1WorkSpace,pt2WorkSpace,pt3WorkSpace]
ptsPrevSpace = [(1024,0),(0,0),(0,1024),(1024,1024)]
ptsNetSpace  = [(300,0),(0,0),(0,300),(300,300)]
print ("ptsWorkSpace = ",ptsWorkSpace)

lastXClickedCam = 0
lastYClickedCam = 0
global newClickCam
def callBackPtsSelCamSpace(event, x, y, flags, param):
    global keyboard
    if event == cv2.EVENT_LBUTTONDOWN:
        print(x , "  " , y)
        global lastXClickedCam
        lastXClickedCam = x
        global lastYClickedCam
        lastYClickedCam = y
        global newClickCam
        newClickCam = True

lastXClickedPrev = 0
lastYClickedPrev = 0
global newClickPrev
def callBackPtsSelPrevSpace(event, x, y, flags, param):
    global keyboard
    if event == cv2.EVENT_LBUTTONDOWN:
        print(x , "  " , y)
        global lastXClickedPrev
        lastXClickedPrev = x
        global lastYClickedPrev
        lastYClickedPrev = y
        global newClickPrev
        newClickPrev = True

width=1920 # image width of the realsense camera. Nominal: 1920
height=1080 # image height of the realsense camera. Nominal: 1080
rsfps=30 # fps target for realsense camera. Nominal: 30

print("Getting pipeline...")
pipeline = rs.pipeline()
print("Getting config...")
config = rs.config()
print("Enabling stream...")
config.enable_stream(rs.stream.color, width, height, rs.format.bgr8, rsfps) # Nominal: 1920x18080 bgr8 30fps
print("Resolving pipeline...")
profile = config.resolve(pipeline)
print("Starting pipeline...")
pipeline.start(config)
num_frames=0

while True :
    frames = pipeline.wait_for_frames()
    color_frame = frames.get_color_frame()
    if not color_frame:
        num_frames=0
        continue
    else:
        num_frames+=1
    if ( num_frames > 10 ):
        break

pipeline.stop()
image_np = np.asanyarray(color_frame.get_data())
image_np = cv2.flip(image_np, -1)

try:
    imgBaseCamSpace = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
except:
    print("Error converting to RGB")

ptsCamSpace = list()
tmpImgCamSpace = imgBaseCamSpace.copy()
for p in ptsCamSpace:
    cv2.circle(tmpImgCamSpace, (p[0], p[1]), 10, (255, 0, 0), 3, 2)
    cv2.line(tmpImgCamSpace, (p[0] + 10, p[1]), (p[0] - 10, p[1]), (255, 0, 0), 1, 1)
    cv2.line(tmpImgCamSpace, (p[0], p[1] + 10), (p[0], p[1] - 10), (255, 0, 0), 1, 1)
cv2.imshow('CamSpace', cv2.cvtColor(tmpImgCamSpace, cv2.COLOR_RGB2BGR))

cv2.setMouseCallback("CamSpace", callBackPtsSelCamSpace)

print('Select corner TOP RIGHT and press a key ' )
cv2.waitKey(0)
pt = [lastXClickedCam,lastYClickedCam]
ptsCamSpace.append(pt)
tmpImgCamSpace = imgBaseCamSpace.copy()
for p in ptsCamSpace:
    cv2.circle(tmpImgCamSpace, (p[0], p[1]), 10, (255, 0, 0), 3, 2)
    cv2.line(tmpImgCamSpace, (p[0] + 10, p[1]), (p[0] - 10, p[1]), (255, 0, 0), 1, 1)
    cv2.line(tmpImgCamSpace, (p[0], p[1] + 10), (p[0], p[1] - 10), (255, 0, 0), 1, 1)
cv2.imshow('CamSpace', cv2.cvtColor(tmpImgCamSpace, cv2.COLOR_RGB2BGR))

print('Select corner TOP LEFT and press a key ' )
cv2.waitKey(0)
pt = [lastXClickedCam,lastYClickedCam]
ptsCamSpace.append(pt)
tmpImgCamSpace = imgBaseCamSpace.copy()
for p in ptsCamSpace:
    cv2.circle(tmpImgCamSpace, (p[0], p[1]), 10, (255, 0, 0), 3, 2)
    cv2.line(tmpImgCamSpace, (p[0] + 10, p[1]), (p[0] - 10, p[1]), (255, 0, 0), 1, 1)
    cv2.line(tmpImgCamSpace, (p[0], p[1] + 10), (p[0], p[1] - 10), (255, 0, 0), 1, 1)
cv2.imshow('CamSpace', cv2.cvtColor(tmpImgCamSpace, cv2.COLOR_RGB2BGR))

print('Select corner BOTTOM LEFT and press a key ' )
cv2.waitKey(0)
pt = [lastXClickedCam,lastYClickedCam]
ptsCamSpace.append(pt)
tmpImgCamSpace = imgBaseCamSpace.copy()
for p in ptsCamSpace:
    cv2.circle(tmpImgCamSpace, (p[0], p[1]), 10, (255, 0, 0), 3, 2)
    cv2.line(tmpImgCamSpace, (p[0] + 10, p[1]), (p[0] - 10, p[1]), (255, 0, 0), 1, 1)
    cv2.line(tmpImgCamSpace, (p[0], p[1] + 10), (p[0], p[1] - 10), (255, 0, 0), 1, 1)
cv2.imshow('CamSpace', cv2.cvtColor(tmpImgCamSpace, cv2.COLOR_RGB2BGR))

print('Select corner BOTTOM RIGHT and press a key ' )
cv2.waitKey(0)
pt = [lastXClickedCam,lastYClickedCam]
ptsCamSpace.append(pt)
tmpImgCamSpace = imgBaseCamSpace.copy()
for p in ptsCamSpace:
    cv2.circle(tmpImgCamSpace, (p[0], p[1]), 10, (255, 0, 0), 3, 2)
    cv2.line(tmpImgCamSpace, (p[0] + 10, p[1]), (p[0] - 10, p[1]), (255, 0, 0), 1, 1)
    cv2.line(tmpImgCamSpace, (p[0], p[1] + 10), (p[0], p[1] - 10), (255, 0, 0), 1, 1)
cv2.imshow('CamSpace', cv2.cvtColor(tmpImgCamSpace, cv2.COLOR_RGB2BGR))

print ("Point selection done")
print ("Doing some math...")

# Computation
TMCamNet    = cv2.getPerspectiveTransform ( np.array(ptsCamSpace, dtype="float32")   , np.array(ptsNetSpace, dtype="float32") )
TMCamPrev   = cv2.getPerspectiveTransform ( np.array(ptsCamSpace, dtype="float32")   , np.array(ptsPrevSpace, dtype="float32") )
TMCamWork   = cv2.getPerspectiveTransform ( np.array(ptsCamSpace, dtype="float32")   , np.array(ptsWorkSpace, dtype="float32") )

TMNetCam    = cv2.getPerspectiveTransform ( np.array(ptsNetSpace, dtype="float32")   , np.array(ptsCamSpace, dtype="float32") )
TMNetPrev   = cv2.getPerspectiveTransform ( np.array(ptsNetSpace, dtype="float32")   , np.array(ptsPrevSpace, dtype="float32") )
TMNetWork   = cv2.getPerspectiveTransform ( np.array(ptsNetSpace, dtype="float32")   , np.array(ptsWorkSpace, dtype="float32") )

TMPrevCam   = cv2.getPerspectiveTransform ( np.array(ptsPrevSpace, dtype="float32")   , np.array(ptsCamSpace, dtype="float32") )
TMPrevNet   = cv2.getPerspectiveTransform ( np.array(ptsPrevSpace, dtype="float32")   , np.array(ptsNetSpace, dtype="float32") )
TMPrevWork  = cv2.getPerspectiveTransform ( np.array(ptsPrevSpace, dtype="float32")   , np.array(ptsWorkSpace, dtype="float32") )

TMWorkCam   = cv2.getPerspectiveTransform ( np.array(ptsWorkSpace, dtype="float32")   , np.array(ptsCamSpace, dtype="float32") )
TMWorkNet   = cv2.getPerspectiveTransform ( np.array(ptsWorkSpace, dtype="float32")   , np.array(ptsNetSpace, dtype="float32") )
TMWorkPrev  = cv2.getPerspectiveTransform ( np.array(ptsWorkSpace, dtype="float32")   , np.array(ptsPrevSpace, dtype="float32") )

print("Starting preview: select a point in the ")

#this may not exist
#imgBaseWorkSpace = cv2.warpPerspective( imgBaseCamSpace, trasfMatrix ,(300,300))

# Preview

imgBasePrevSpace   = cv2.warpPerspective( imgBaseCamSpace, TMCamPrev , (1024,1024) )
imgBaseNetSpace    = cv2.warpPerspective( imgBaseCamSpace, TMCamNet  , (300,300) )

cv2.imshow('PrevSpace', cv2.cvtColor ( imgBasePrevSpace , cv2.COLOR_RGB2BGR ) )
#cv2.imshow('NetSpace' , cv2.cvtColor ( imgBaseNetSpace  , cv2.COLOR_RGB2BGR ) )
newClickCam=False
newClickPrev=False
print("Click image for testing, any key to continue")
cv2.setMouseCallback("PrevSpace", callBackPtsSelPrevSpace)

while True:
    if ( cv2.waitKey(10)>0 ):
        break
    else:
        if (newClickPrev):
            tmpImg = imgBasePrevSpace.copy()
            pPrev=(lastXClickedPrev,lastYClickedPrev)
            cv2.circle(tmpImg, (pPrev[0], pPrev[1]), 10, (255, 0, 0), 3, 2)
            cv2.line  (tmpImg, (pPrev[0] + 10, pPrev[1]     ), (pPrev[0] - 10, pPrev[1]     ), (255, 0, 0), 1, 1)
            cv2.line  (tmpImg, (pPrev[0]     , pPrev[1] + 10), (pPrev[0]     , pPrev[1] - 10), (255, 0, 0), 1, 1)
            cv2.imshow('PrevSpace', cv2.cvtColor(tmpImg, cv2.COLOR_RGB2BGR))
            pPrevH = (pPrev[0] , pPrev[1] , 1.0)
            print(pPrevH)
            pWorkH = np.dot(TMPrevWork,pPrevH)
            pWork  = (pWorkH[0] / pWorkH[2] , pWorkH[1] / pWorkH[2] )
            print(pWork)
            #print("Punto robot: " , round(pWork[0]) , " " , round(pWork[1]) )
            newClickPrev = False


print("Saving mats")
basedir = '/home/tera/T-Brain/data/calibration/'
np.save(basedir+'TMCamNet'  , TMCamNet  )
np.save(basedir+'TMCamPrev' , TMCamPrev )
np.save(basedir+'TMCamWork' , TMCamWork )

np.save(basedir+'TMNetCam'  , TMNetCam  )
np.save(basedir+'TMNetPrev' , TMNetPrev )
np.save(basedir+'TMNetWork' , TMNetWork )

np.save(basedir+'TMPrevCam' , TMPrevCam )
np.save(basedir+'TMPrevNet' , TMPrevNet )
np.save(basedir+'TMPrevWork', TMPrevWork)

np.save(basedir+'TMWorkCam' , TMWorkCam )
np.save(basedir+'TMWorkNet' , TMWorkNet )
np.save(basedir+'TMWorkPrev', TMWorkPrev)

print("Cleaning env...")
cv2.destroyAllWindows()
print("DONE!!!")
sys.exit(0)

